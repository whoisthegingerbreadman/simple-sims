import numpy as np
from collections import deque
class Instrument:
    def __init__(self,name,type,initialization,connections=[],noise=0,delay=0):
        self.type = type
        self.value_memory = deque(maxlen=delay+1)
        self.value = initialization
        self.true_value = initialization
        self.controlled=False
        self.name = name
        self.history = {'value':[],'true_value':[],'failing':[],'step_number':[]}
        self.failing = False
        self.last_update_time = 0
        self.noise = noise
    def update_value(self,value,noise=None):
        
        self.last_update_time = value['step_number']
        
        self.history['value'].append(self.get_value())
        self.history['failing'].append(self.failing)
        self.history['true_value'].append(self.get_true_value())
        self.history['step_number'].append(self.last_update_time)
        self.true_value = value['value']
        self.value_memory.append(self.true_value)
        if not self.failing:
            if noise is None:
                noise = self.noise
            if self.type == "real":
                self.value = self.value_memory[0] + np.random.randn()*noise
            else:
                self.value = self.true_value
        else:
            if self.type == "real":
                
                self.value = self.value_memory[0] + np.random.randn()*noise
            else:
                pass
    
    def update_failure(self,value):
        self.failing = value
    
    def get_value(self):
        return self.value
    
    def get_true_value(self):
        return self.true_value
    
    def set_value(self,true_value):
        self.true_value = true_value
        self.value =true_value
        

class ControlledInstrument(Instrument):
    def __init__(self, name, type, initialization, connections=[],noise = 0,control_function=lambda x:x['value'],delay=0):
        super().__init__(name, type, initialization, connections=connections,noise=noise,delay=delay)
        self.controlled=True
        self.control_function = control_function
    
    def set_control_function(self,control_function):
        self.control_function = control_function
    
    def update_value(self, value,noise=None):

        self.true_value = self.control_function(value)
        self.last_update_time = value['step_number']
        self.history['value'].append(self.get_value())
        self.history['failing'].append(self.failing)
        self.history['true_value'].append(self.get_true_value())
        self.history['step_number'].append(self.last_update_time)
        if not self.failing:
            if noise is None:
                noise = self.noise
            if self.type == "real":
                self.value = self.true_value + np.random.randn()*noise
            else:
                self.value = self.true_value
        else:
            if self.type == "real":
                self.value = self.value + np.random.randn()*noise
            else:
                pass



