import numpy as np

## Random Control Functions
def make_random_sinusoidal_function(amplitude_scale = 10,period_scale = 100,core_var="x['step_number']"):
    amplitude = np.random.random()*amplitude_scale

    period = 2*np.pi/(np.random.random()*period_scale)
    phase = np.random.random()*np.pi
    return f"{amplitude} * np.sin({period}*{core_var} + {phase})"

def make_random_polynomial_function(scales = [1,1],core_var="x['step_number']"):
    out = ""
    for order,scale in enumerate(scales):
        out+= f"{scale*np.random.random()}*{core_var}**{order}+"
    return out[:-1]

def make_random_inverse_polynomial_function(scales = [0,1],core_var="x['step_number']"):
    out = ""
    for order,scale in enumerate(scales):
        if order==0:
            out+= f"{scale*np.random.random()}+"
        else:
            out+= f"{scale*np.random.random()}*{core_var}**{order**-1}+"
    return out[:-1]

def make_random_exponential_function(scale=1,exp=1):
    out = ""
    pass

def make_random_binomial_function(p=.5):
    return f"np.random.random()<{p}"

#Make the control lambdas
def make_lambda(input_string):
    return f"lambda x: {make_random_sinusoidal_function()}"


make



if __name__ == "__main__":
    print(make_random_sinusoidal_function())
    print(make_random_polynomial_function([3,2,2,2,2,1,1,1,]))
    print(make_random_inverse_polynomial_function([0,1,1,1]))
