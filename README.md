# ProGraph Time Series Synthesis

ProGraph is made to extract causal relationships between elements in an unknown set of time series.

This is the Time Series Synthesis component of that system that we need to being testing the prograph model.  


## The Idea for this Part

When we are training models that are made to detect causation there are generally three kind of systems.

1. A system that whose causal structure is known to exist and is understood.
2. A system whose causal structure is known to exist but not understood.
3. A system whose causal structure is not known to exist.

In the real world we are more likely than not to encounter the 3rd option. Even in systems whose structure we think we understand it is rare for us to be able to find a structure that is completely described. This makes it very difficult to know if the system has inferred the correct structure of the system or it has simply found a structure that seems to work. For training a meta model to derive models this is an important distinction.

This module is made to allow for the production of synthetic arbitrary markov chain based time series that produces both the time series and the structure of the model.


## Structure of a Causal Graph

The causal graph is composed of nodes and edges just like a normal graph. There are two kinds of nodes.

1. Instrument Node
2. Functional Node


### Instrument Nodes 

An instrument node is the juice of our data. It is the time series in node form. All the data that we have comes from these Instrument nodes. Within instrument nodes there are 2 subvarieties:

1. Controlled Instrument Nodes
2. Uncontrolled Instrument Nodes

The controlled instrument nodes are controlled in the sense that they are the producers of our root data. The data that comes out of the controlled instruments flow downwards to all the other nodes and mixes in with everything down the line to produce the full system dynamics.

The Uncontrolled Instrument Nodes take in input from functional nodes to produce an updated value. 

### Functional Nodes

Functional Nodes are nodes that take in values from one or many Instrument Nodes and output a value to an instrument node. Functional nodes represent the response from one or many timeseries to another. These are the actors by which causality functions.


## Features To Make

- [x] Models are running through sim_runner.py
- [x] Added Noise option to the instruments
- [x] Controlled Nodes
- [ ] Failures on Controlled Nodes
- [x] Add time delay optionality on the functional node
- [ ] Dynamic Model Structure
    - [ ] History of Model Structure
- [x] Make functions generalizable to any variable names by configuring
- [ ] Make a time series stream instead of only output a set of offline observations
- [ ] Random Structure Generation
- [ ] Deal with cycles.
    - Cycles aren't always bad. Look at D-Latch. We still want to be able to model a d latch. 
- [ ] Make a random config generator
    - [ ] Make a random function generator


## Contributers

1. Aviv M. Elbag



