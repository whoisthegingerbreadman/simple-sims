import json
import sys
import os
import logging

def parse_sim_def(sim_name):
    sim_name = sim_name
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(dir_path,"sims",sim_name+".json"),"r") as f:
        sim =json.load(f)

    num_steps = sim["num_steps"]
    debug_mode = sim["debug"]
    save_outputs = sim["save_outputs"]

    nodes = sim.get("nodes",[])
    edges = sim.get("edges",[])

    required_modules = []
    env = sim.get("env",{})
    for edge in edges:
        module_to_add = edge["function"].split(".")
        if module_to_add not in required_modules:
            required_modules.append(module_to_add)
    return num_steps,debug_mode,save_outputs,nodes,edges,required_modules,env

