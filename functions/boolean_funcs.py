import numpy
def sin(amplitude=1,period=1,offset=0):
    return "lambda x: amplitude*np.sin(2*np.pi/period * x['step_number'])"

def or_gate_B1_B2__B3(input_args,env):
    B1 = input_args["B1"]
    B2 = input_args["B2"]
    return {"B3": B1 or B2}

def xor_gate_B1_B2__B3(input_args,env):
    B1 = input_args["B1"]
    B2 = input_args["B2"]
    return {"B3": B1 ^ B2}


def and_gate_B1_B2__B3(input_args,env):
    B1 = input_args["B1"]
    B2 = input_args["B2"]
    return {"B3": B1 and B2}

def not_gate_B1__B2(input_args,env):
    B1 = input_args["B1"]
    return {"B2":~B1}
