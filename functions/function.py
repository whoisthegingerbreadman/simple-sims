class Function:
    def __init__(self,input_types,function):
        self.input_types = input_types
        self.function = function
    def __call__(self,**input):
        return self.function(**input)

