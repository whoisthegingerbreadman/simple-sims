def perfect_valve_with_ambient_temperature_T1_V1__T2(input_args,env):
    dt = env["dt"]
    AMBIENT_TEMPERATURE = env["ambient_temperature"]
    T1 = input_args["T1"]
    V1 = input_args["V1"]
    T2 = input_args["T2"]
    if V1:
        return {"T2":T2+(T1-T2)*dt}
    else:
        return {"T2":T2+(AMBIENT_TEMPERATURE-T2)*dt}


def what_perfect_valve_with_ambient_temperature_T1_V1__T2(input_args,env):
    dt = env["dt"]
    AMBIENT_TEMPERATURE = env["ambient_temperature"]
    T1 = input_args["T1"]
    V1 = input_args["V1"]
    T2 = input_args["T2"]
    if V1:
        return {"T2":T2*.999}
    else:
        return {"T2":T2*1.001}

# def perfect_valve_with_ambient_temperature_T1_V1__T2(input_args,env):
#     dt = env["dt"]
#     AMBIENT_TEMPERATURE = env["ambient_temperature"]
#     T1 = input_args["T3"]
#     V1 = input_args["V2"]
#     T2 = input_args["T4"]
#     if V1:
#         return {"T4":T2+(T1-T2)*dt}
#     else:
#         return {"T2":T2+(AMBIENT_TEMPERATURE-T2)*dt}