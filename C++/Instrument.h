#ifndef INSTRUMENT_DEF
#define INSTRUMENT_DEF

#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

template<class T>
struct History{
    std::unordered_map<int, std::vector<T>> value;
    std::unordered_map<int, std::vector<T>> true_value;
    std::unordered_map<int, bool> failing;
    std::vector<int> step_number;
};

template <class T>
class Instrument{
    private:
        std::string name;
        T value;
        T true_value;
        bool controlled;
        bool failing=false;
        float last_update_time = 0;
        int num_steps = 0;
        History<T> history; 
    public:
        Instrument(std::string n,T init,T true_val,bool cont = false):
        name(n),
        value(init),
        true_value(true_val),
        controlled(cont){
        }
        
        std::string get_name(){
            return name;
        }

        T get_current_value(){
            return value;
        }

        T get_true_value(){
            return true_value;
        }
        
        void step(float dt){
            num_steps++;
            last_update_time+=dt;
            history.step_number.emplace_back(num_steps);
        }

        void summarize(){
            std::cout << "Name: " << name << std::endl;
            std::cout << "True Value: " << value << std::endl;
            std::cout << "Current Value: " << true_value << std::endl;
            for(int i : history.step_number){
                std::cout<<i << "  ";
            }
        }
};
#endif
