class Connection:
    def __init__(self,source_variables=None,sink_variable = None,function=None,delay = 0,env={}):
        self.source_variables = source_variables
        self.sink_variables = sink_variable
        self.function = function
        self.delay = delay
        self.env = env
    def get_all_variables(self):
        out = self.source_variables
        out.update(self.sink_variables)
        return out
    def set_function(self,function):
        self.function = function
    def apply(self,values):
        update = self.function(values,self.env)
        out = {}
        for k,v in update.items():
            out[self.sink_variables[k]]= v
        return out
