import logging
from simple_sims.graph_utils import make_functional_graph
from graph_utils import *

class System:
    def __init__(self,name):
        self.name = name
        self.instruments = {'controlled':{},'uncontrolled':{}}
        self.connections = {}
        self.functional_nodes ={}
        
    def add_instrument(self,instrument):
        assert instrument.name not in self.instruments
        logging.info("Adding instrument {}".format(instrument.name))
        if instrument.controlled:  
            self.instruments['controlled'][instrument.name] = instrument
        else:
            self.instruments['uncontrolled'][instrument.name] = instrument
    
    def add_multiple_instruments(self,instruments):
        for instrument in instruments:
            self.add_instrument(instrument)

    def add_connection(self,name,connection):
        assert name not in self.connections
        self.connections[name] = connection
    
    def add_connections(self,connections):
        for name,connection in connections:
            self.add_connection(name,connection) 
    
    def from_graph(self,graph):
        self.functional_graph,functional_nodes,control_nodes = make_functional_graph(graph)
        for node in graph.nodes:
            self.add_instrument(node)
        
    




