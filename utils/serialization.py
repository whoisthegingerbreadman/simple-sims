import numpy as np
import json
from collections import namedtuple
from enum import Enum
SubChannel = namedtuple("subchannel",("value","is_time_series"))
Channel = namedtuple("channel",("value","true_value","time_stamp","failing","dtype","size","name"))
SampleWindow = namedtuple("window",("input","label"))


class DataVar(Enum):
    TRAIN = 0
    TEST = 1
    VALIDATION = 2

class Data:
    """
    An object to represent the data that is output by the simple sims generator.
    This will likely need to be modified when the simple sims generator is not useful
    anymore or we are using real data. 
    """
    def __init__(self,data:dict,data_var,name="Sample Data"):
        self.name = name
        self.start_time = np.inf
        self.end_time = -np.inf
        self.channels = {}
        self.channel_names = []
        for key,data in data.items():
            self.channel_names.append(key)
            try:
                dtype = type(data['value'][0])
            except:
                dtype = None
            self.channels[key] = Channel(
                SubChannel(data['value'],True),
                SubChannel(data['true_value'],True),
                SubChannel(data["step_number"],True),
                SubChannel(data["failing"],True),
                SubChannel(dtype,False),
                SubChannel(len(data['value']),False),
                SubChannel(key,False)
            )
            try:
                assert self.channels[key].size.value == len(self.channels[key].true_value.value)
                assert self.channels[key].size.value == len(self.channels[key].time_stamp.value)
                assert self.channels[key].size.value == len(self.channels[key].failing.value)
                assert self.channels[key].dtype.value == type(self.channels[key].true_value.value)
            except:
                Exception(f"NAME: {key} \n SIZE: {self.channels[key].size.value} \n TRUE_VALUE SIZE: {len(self.channels[key].true_value.value)} \n TIME STAMP SIZE: {len(self.channels[key].time_stamp.value)} \n FAILING SIZE: {len(self.channels[key].failing.value)}")
            try:
                self.end_time = max(self.end_time,max(self.channels[key].time_stamp.value))
                self.start_time = min(self.start_time,min(self.channels[key].time_stamp.value))
            except:
                pass
        if self.end_time == -np.inf:
            self.end_time =0
        if self.start_time == np.inf:
            self.start_time = 0
        self.variety = data_var
        self.num_channels = len(self.channels)

    def __repr__(self) -> str:
        out_string =  f"Instrument: {self.name} \n Num Channels:{self.num_channels} \n Channel IDs: {self.channel_names} \n Channels: \n"
        for channel in self.channels.values():
            out_string += f"\n\t Name: {channel.name.value} \n\t\t Size: {channel.size.value} \n\t\t Dtype: {channel.dtype.value} \n"
        return out_string

    def get_window_at_time(self,start_time:int,size:int,input_slice:slice,label_slice:slice)->SampleWindow:
        inputs = {key:self.channels[key].value.value[start_time:start_time+size][input_slice] for key in self.channels if self.channels[key].value.is_time_series}
        labels = {key:self.channels[key].value.value[start_time:start_time+size][label_slice] for key in self.channels if self.channels[key].value.is_time_series}

        return SampleWindow(inputs,labels)

        
def make_empty_data(name:str,data_var:DataVar) -> Data:
    data = {"Dummy":{"value":[],"true_value":[],"step_number":[],"failing":[]}}
    return Data(data,data_var)


class WindowGenerator:
    """
    A window generator takes in a set of objects and from there allows you to slice the 
    data in a way that is useful and convenient for the model you plan on using. 

    """
    def __init__(self,input_width:int,label_width:int,train_data:Data,test_data:Data=None,val_data:Data = None,offset:int=0):
        self.input_width = input_width
        self.label_width = label_width
        

        self.offset = offset
        
        ## The data must be of the right variety. 
        self.train_data = train_data
        assert self.train_data.variety == DataVar.TRAIN
        
        if test_data is not None:
            self.test_data = test_data
            assert self.test_data.variety == DataVar.TEST
        else:
            self.test_data = make_empty_data("Empty-Test",DataVar.TEST)
        
        if val_data is not None:
            self.val_data = val_data
            assert self.val_data.variety == DataVar.VALIDATION
        else:
            self.val_data = make_empty_data("Empty-Validation",DataVar.VALIDATION)

        ## The total width must be greater than the label width. That is, the input width has to have at least one value.
        self.total_width = self.input_width + self.offset + self.label_width

        if self.total_width<self.label_width:
            raise Exception("Error: Total Window Width is shorter than the label width")
        ## Adding some convenience variables. 
        self.start_label_index = self.total_width-self.label_width
        self.start_input_index = 0
        self.end_label_index = self.total_width
        self.end_input_index = self.input_width

        self._input_slice = slice(0,self.input_width)
        self._label_slice = slice(self.start_label_index,None)
        self.indices = np.arange(self.total_width)
        self.input_indices = self.indices[self._input_slice]
        self.label_indices = self.indices[self._label_slice]

    def __repr__(self):
        out_string= f"Window Width: {self.total_width} \n Input Width: {self.input_width} \n Label Width: {self.label_width} \n Input Indices: {self.input_indices} \n Label Indices: {self.label_indices}"
        return out_string


    def sample(self,n:int=1,data_var:DataVar = DataVar.TRAIN,seed = 1)->list:
        """
        Gets a sample window output of the data
        n: The number of samples to draw
        data_type: The data type could be 
        """
        
        if data_var == DataVar.TRAIN:
            data_of_interest = self.train_data
        elif data_var == DataVar.TEST:
            data_of_interest = self.test_data
        elif data_var == DataVar.VALIDATION:
            data_of_interest = self.val_data
        
        if data_of_interest is None:
            raise Exception(f"Error: The data of variation {data_var} either doesn't exist or is not defined.")
        else:
            np.random.seed(seed)
            start_time_samples = np.random.randint(data_of_interest.start_time,data_of_interest.end_time,n)
            samples = [data_of_interest.get_window_at_time(start_time,self.total_width,self._input_slice,self._label_slice,) for start_time in start_time_samples]
            return samples


        

def _get_history(location,data_var=DataVar.TRAIN,name="Sample_Data"):
    with open(location, 'r') as f:
        data = json.load(f)    
    return Data(data,data_var,name=name)


        
    
            

