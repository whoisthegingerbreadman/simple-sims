import networkx as nx
from copy import deepcopy
def make_functional_graph(graph):
    directional_neighbors = get_all_directional_neighbors(graph)
    functional_graph = nx.DiGraph()
    control_nodes = []
    functional_nodes = []
    for node,connections in directional_neighbors.items():
        if len(connections) ==0:
            root_node = True
            node_color= "red"
            control_nodes.append(node.name)
        else:
            root_node = False
            node_color = "pink"
        functional_graph.add_node(node.name,node_kind="instrument",root_node=root_node,node_color=node_color)
    for node in directional_neighbors:
        connections = directional_neighbors[node]   
        if len(connections) == 0:
            continue
        else:
            function_node_name = "_".join([conn.name for conn in connections]) + "_" + node.name
            functional_graph.add_node(function_node_name,node_kind="functional",node_color="green")
            functional_graph.add_edge(function_node_name,node.name)
            functional_nodes.append(function_node_name)
            for conn in connections:
                functional_graph.add_edge(conn.name,function_node_name)
    return functional_graph,functional_nodes,control_nodes
        
def get_all_directional_neighbors(graph):
    edges = []
    nodes = {node:[] for node in graph.nodes}
    for e in graph.edges:
        nodes[e[1]].append(e[0])
    return nodes


def propagate_graph():
    functional_nodes_requirements = {name:deepcopy(conn.source_variables) for name,conn in functional_node_functions.items()}
    edges_to_explore = [edge for edge in FG.edges if edge[0] in control_nodes]
    for control_node in control_nodes:
        input_args = {"value":instruments[control_node].value,"step_number":step_number}
        instruments[control_node].update_value(control_node_inputs[control_node](input_args))

    while edges_to_explore:
        cur_edge = edges_to_explore.pop()
        if cur_edge[0] in instruments:
            if cur_edge[1] in functional_nodes_requirements:
                functional_nodes_requirements[cur_edge[1]].remove(cur_edge[0])
                if len(functional_nodes_requirements[cur_edge[1]]) == 0:
                    cur_connection = functional_node_functions[cur_edge[1]]
                    all_required_variables = cur_connection.get_all_variables()
                    input_args = {var:instruments[var].value for var in all_required_variables}
                    output_args = cur_connection.apply(input_args)
                    for var,value in output_args.items():
                        instruments[var].update_value(value)
            edges_to_explore.extend([edge for edge in FG.edges if edge[0] is cur_edge[1]])
        elif cur_edge[0] in functional_nodes:
            if len(functional_nodes_requirements[cur_edge[0]])==0:
                edges_to_explore.extend([edge for edge in FG.edges if edge[0] is cur_edge[1]])
            else:
                edges_to_explore = [cur_edge] + edges_to_explore