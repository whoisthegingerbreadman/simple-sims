
from instrument import Instrument,ControlledInstrument
from connection import Connection
from graph_utils import *
import numpy as np
from pyvis.network import Network

import networkx as nx
import matplotlib.pyplot as plt
from copy import deepcopy
import sys
import read_config
import logging

LOGGING_LEVEL = logging.INFO

logging.basicConfig(level=LOGGING_LEVEL)

def run_sim(functional_node_functions,control_node_inputs,G,instruments,env,num_steps,debug_mode=False,save_outputs=True):
    for control_node_name,func in control_node_inputs.items():
        instruments[control_node_name].set_control_function(func)

    #Build a functional graph that includes the functional nodes. The edges have all the information necessary to build the functional
    #Nodes just fyi.
    FG,functional_nodes,control_nodes = make_functional_graph(G)

    ### PROPAGATE ALONG THE GRAPH 

    for step_number in range(num_steps):
        #For each functional node the required non functional nodes need to be filled before the functional node is run.
        # To make sure we have updated each node that is required by a functional node we list out each of the required 
        # nodes and remove them as they are filled. 
        functional_nodes_requirements = {name:list(conn.source_variables.values()) for name,conn in functional_node_functions.items()}
        edges_to_explore = [edge for edge in FG.edges if edge[0] in control_nodes]
        for control_node in control_nodes:
            input_args = {"value":instruments[control_node].value,"step_number":step_number}
            instruments[control_node].update_value(input_args)
        
        # While there are still edges that haven't been traversed the graph hasn't been updated for the step
        while edges_to_explore:
            cur_edge = edges_to_explore.pop()
            # If the edge is a instrument -> functional node then we want to fill the node for the instrument. IF the functional node requirement
            # is now satisfied then we will update the functional node.
            if cur_edge[0] in instruments:
                if cur_edge[1] in functional_nodes_requirements:
                    if cur_edge[0] in functional_nodes_requirements[cur_edge[1]]:
                        functional_nodes_requirements[cur_edge[1]].remove(cur_edge[0])
                    
                        if len(functional_nodes_requirements[cur_edge[1]]) == 0:
                            cur_connection = deepcopy(functional_node_functions[cur_edge[1]])
                            all_required_variables = cur_connection.get_all_variables()
                            input_args = {var_local:instruments[var_global].get_true_value() for var_local,var_global in all_required_variables.items()}
                            output_args = cur_connection.apply(input_args)
                            for var,value in output_args.items():
                                instruments[var].update_value({'step_number':step_number,"value":value})
                edges_to_explore.extend([edge for edge in FG.edges if edge[0] is cur_edge[1]])
            #If the edge is a functional node -> instrument node then we want to add all the edges from the functional node to it's connecting
            # instruments if the functional node has been satisfied. Else replace the edge and put it in the back of the list to be visited last.
            # after all other nodes have been visited. 
            elif cur_edge[0] in functional_nodes:
                if len(functional_nodes_requirements[cur_edge[0]])==0:
                    edges_to_explore.extend([edge for edge in FG.edges if edge[0] is cur_edge[1]])
                else:
                    edges_to_explore = [cur_edge] + edges_to_explore

    #update control nodes every step
    plt.figure(figsize=(6,6))
    if len(instruments)>6:
        for instrument in instruments:
            times = instruments[instrument].history['step_number']
            values = instruments[instrument].history['value']

            if instrument not in control_nodes:
                plt.plot(times,values,label=instrument,color="blue")   
            else:
            
                plt.plot(times,values,label=instrument,color="red")
        plt.legend()   
        plt.show()
    else:
        fig,axes = plt.subplots(len(instruments))
        axes = axes.flatten()
        for i,instrument in enumerate(instruments):
            times = instruments[instrument].history['step_number']
            values = instruments[instrument].history['value']
            axes[i].set_title(instrument)
            if instrument not in control_nodes:
                axes[i].plot(times,values,label=instrument,color="blue")   
            else:
            
                axes[i].plot(times,values,label=instrument,color="red")
        plt.show()


        

    if debug_mode:
        print("Displaying graph")
        node_colors = [FG.nodes[n]['node_color'] for n in FG.nodes]
        g = Network(directed=True)
        g.from_nx(FG)
        g.show("graph.html")
        #nx.draw(FG,node_color=node_colors)
        #plt.show()

    if save_outputs:
        import json
        all_data = {}
        for instrument_name in instruments:
            all_data[instrument_name] = {
                "value":instruments[instrument_name].history['value'],
                "step_number":instruments[instrument_name].history['step_number'],
                "true_value":instruments[instrument_name].history['true_value'],
                "failing":instruments[instrument_name].history['failing'],
                "kind":instruments[instrument_name].type}
            

        with open("history.json","w") as f:
            json.dump(all_data,f)



def make_nodes_and_instruments(nodes,G):
    instruments = {} # The instruments data sctructure. instrument name -> Instrument
    control_node_inputs = {} # Instruments that are controlled.
    for node in nodes: # Construct the nodes
        if node["type"] == "controlled":
            instruments[node["name"]] = ControlledInstrument(node["name"],node["dtype"],node["initialization"],noise=node["noise"],delay=1)
            control_node_inputs[node["name"]] = eval(node["control_function"])
        elif node["type"] == "uncontrolled":
            instruments[node["name"]] = Instrument(node["name"],node["dtype"],node["initialization"],noise=node["noise"],delay=node["delay"])
        G.add_node(instruments[node["name"]])
    return G,instruments,control_node_inputs

def make_edges(edges,G,env):
    functional_node_functions = {} # functional nodes represents how the nodes interact with each other. Underscore delimited 
    for edge in edges: #Construct the edges
        for source_variable in edge["source_variables"].values():
            for sink_variable in edge["sink_variables"].values():
                G.add_edge(instruments[source_variable],instruments[sink_variable])
        name = '_'.join(edge['source_variables'].values()) + '_' + '_'.join(edge["sink_variables"].values())
        functional_node_functions[name] = Connection(edge['source_variables'],edge['sink_variables'],eval(edge['function']),env=env)
        
    return G,functional_node_functions


###INPUTS MAKE THE GRAPH AND SET THE PARAMETERS HERE

sim_name = sys.argv[1] ## Take in the sim definition name

NUM_STEPS,DEBUG,SAVE_OUTPUTS,nodes,edges,required_modules,env = read_config.parse_sim_def(sim_name) #Parses the sim definition
if DEBUG:
    print("num steps: {}".format(NUM_STEPS))

for module in required_modules: #Imports the modules that are required for the functional definitions of the nodes and the edges
    if DEBUG:
        print("Running: ")
        print("from functions import {}".format(module[0]))
    exec("from functions import {}".format(module[0]))

G = nx.DiGraph()  # A directional graph with just instrument nodes that define the system.
G,instruments,control_node_inputs = make_nodes_and_instruments(nodes, G)
G,functional_node_functions = make_edges(edges,G,env)
logging.info(f"Number of Edges: {len(G.edges)}")
logging.info(f"Number of Nodes: {len(G.nodes)}")
logging.info(f"Number of Controlled Nodes: {len(control_node_inputs)}")

run_sim(functional_node_functions,control_node_inputs,G,instruments,env,num_steps=NUM_STEPS,debug_mode=DEBUG,save_outputs=SAVE_OUTPUTS)

#####################################################################
